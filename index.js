
function verConocimientos() {
    var element = document.getElementById('conocimientos');

    if (element.classList.contains("active")){
        element.classList.remove("active");
    } else{
        element.classList.add("active");
        document.getElementById('experience').classList.remove("active");
        document.getElementById('studies').classList.remove("active");
        document.getElementById('actividades').classList.remove("active");
    }
}

function verExperiencia() {
    var element = document.getElementById('experience');

    if (element.classList.contains("active")){
        element.classList.remove("active");
    } else{
        element.classList.add("active");
        document.getElementById('conocimientos').classList.remove("active");
        document.getElementById('studies').classList.remove("active");
        document.getElementById('actividades').classList.remove("active");
    }
}
  
function verEstudios() {
    var element = document.getElementById('studies');
    
    if (element.classList.contains("active")){
        element.classList.remove("active");
    } else{
        element.classList.add("active");
        document.getElementById('conocimientos').classList.remove("active");
        document.getElementById('experience').classList.remove("active");
        document.getElementById('actividades').classList.remove("active");
    }
}

function verActividades() {
    var element = document.getElementById('actividades');

    if (element.classList.contains("active")){
        element.classList.remove("active");
    } else{
        element.classList.add("active");
        document.getElementById('conocimientos').classList.remove("active");
        document.getElementById('experience').classList.remove("active");
        document.getElementById('studies').classList.remove("active");
    }
}

function verNombreConsola(){
    var nombre = document.getElementById("name").innerHTML;
    for (var i=1; i<=100; i++){
        console.log(nombre); 
    }
}